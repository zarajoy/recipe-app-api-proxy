# Recipe App API Proxy

MGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - port to listen on (default : `8080`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
